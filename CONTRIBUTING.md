Contributing to Inkscape-web
===========================

Inkscape's creation and development, as an Open Source project, is a community effort! <br>
It takes the time, knowledge, skills, motivation and passions of the whole community, to make Inkscape great. 
There are many ways to contribute to the Inkscape project, whether you have technical, coding or development skills, or not
Here are some ways to contribute to Inkscape-web:

* Write core inkscape code.
* Write Inkscape extensions.
* Packaging Inkscape.
* Website development and design.
* Testing
* Bugs
* Translations
* Promote

Developing Inkscape-web
======================
If you are a coder/programmer and have some knowledge of web development , grab the code and start hacking on whatever draws your attention.  
Inkscape-web is written in Python using the Django framework. It is a good idea to have a familiarity with Django and a fairy healthy experience with Python before starting code development on the website:
* To learn Python, you can visit online classes. [Python](http://www.codecademy.com/en/tracks/python)
* To get to know Django, see this tutorial. [Django](http://tutorial.djangogirls.org/)

Send in a patch when you're happy with it and ready to share your efforts with others. 
We take contributions very seriously and follow the principle of "patch first, discuss later", so it is highly likely your efforts will appear in the development codebase swiftly.  
Here are some important links: 

* [The published website](https://inkscape.org/)- The website seen by users and used by everyone to find resources.
* [The staging website (testing)](http://staging.inkscape.org/) - (currently unavailable)
* [The project on Launchpad](https://launchpad.net/inkscape-web/bugs) - (only bug reports created before February 2017)
* [The project on gitlab](https://inkscape.org/develop/debugging/) - (code repository and bug reports created since February 2017)

Local Development
================
Please reference the README for information on building Inkscape-web

For more information, you can visit the Inkscape-wiki [here](https://wiki.inkscape.org/wiki/index.php/WebSite#Website_Development)