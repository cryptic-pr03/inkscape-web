#
# Copyright 2021 Martin Owens
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Generate a list of reminders and cache them to a file.
"""

import os
import sys
import json

from django.conf import settings
from django.utils.timezone import now
from django.core.serializers.json import DjangoJSONEncoder

from ..base_command import BaseCommand, REMINDER_FILE

from calendars.models import Event

class Command(BaseCommand):
    help = __doc__

    def function(self):
        existing = self.get_reminders()
        reminders = dict(self.generate_reminders())

        # New id in the mix
        if set(reminders) != set(existing):
            self.msg("Events have changed, regenerating cache.", '~')
            return self.save_reminders(reminders)

        changes = set()
        for key in reminders:
            this = existing[key]
            that = reminders[key]
            if existing[key] != reminders[key]:
                changes.add(key)

        if changes:
            changes = ",".join(list(changes))
            self.msg(f"Event(s) {changes} have changed, regenerating.", '+')
            return self.save_reminders(reminders)

    def save_reminders(self, reminders):
        with open(REMINDER_FILE, 'w') as fhl:
            fhl.write(json.dumps(reminders, indent=2, cls=DjangoJSONEncoder))

    def generate_reminders(self):
        for event in Event.objects.filter(reminders__isnull=False).exclude(reminders=''):
            yield (str(event.pk), [dt for x, dt in event.get_reminders()])
