{% load i18n %}
{% blocktrans with recipient=alert.user %}Dear {{ recipient }},{% endblocktrans %}
 
{% blocktrans with user=instance.user team=instance.team %}User '{{ user }}' has requested to join team {{ team }}.{% endblocktrans %}

{% blocktrans %}Visit User page{% endblocktrans %}: {{ site }}{{ instance.user.get_absolute_url }}
{% blocktrans %}Visit Team page{% endblocktrans %}: {{ site }}{{ instance.team.get_absolute_url }}
{% blocktrans %}Approve Request{% endblocktrans %}: {{ site }}{{ instance.get_absolute_url }}
