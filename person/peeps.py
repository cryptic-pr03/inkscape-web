#
# Copyright 2014, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Customise the user model"""

import random

from django.contrib.staticfiles.templatetags.staticfiles import static

# These numbers should be the same as those in `person/static/peeps/peep-{x}.svg`
PEEPS = [1, 105]

class RandomState:
    """Seeded random number without breaking the internal seed"""
    def __init__(self, seed=1):
        self.state = random.getstate()
        self.seed = seed
      
    def __enter__(self):
        random.seed(a=self.seed)
        return random
  
    def __exit__(self, *args, **kargs):
        random.setstate(self.state)

def get_peep_for(username):
    """Returns a peep static url for the given username."""
    with RandomState(seed=username):
        return static('peeps/peep-%d.svg' % random.randint(*PEEPS))
