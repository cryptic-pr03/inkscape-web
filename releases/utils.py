#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Release utils"""


from bs4 import BeautifulSoup
from django.templatetags.static import static

from resources.utils import url_filefield
from .models import ReleaseFile

ID_FIELD = 'data-release-media-id'
BROKEN_IMAGE = static('images/releases/broken_image.svg')

def local_content(html, release=None, uploader=None):
    """Move remote content (images etc) to local content."""
    soup = BeautifulSoup(html, 'html5lib')
    # Remove some bad tags we don't want
    for tag in soup(['iframe', 'script', 'object', 'embed']):
        tag.extract()
    # Check html for image tags.
    for tag in soup(['img']):
        rel_file = None
        img_id = tag.get(ID_FIELD, None)
        if img_id:
            try:
                # Refresh the url (just in case)
                rel_file = ReleaseFile.objects.get(pk=img_id)
            except ReleaseFile.DoesNotExist:
                pass # Broken Image
        else:
            img_url = tag['src']
            try:
                rel_file = ReleaseFile.objects.get(original_url=img_url)
            except ReleaseFile.DoesNotExist:
                if len(img_url) < 500:
                    rel_file = ReleaseFile(
                        disk_file=url_filefield(img_url),
                        original_url=img_url,
                        first_seen=release,
                        uploaded_by=uploader)
                    rel_file.save()

        if rel_file is not None:
            tag['src'] = rel_file.disk_file.url
            tag[ID_FIELD] = rel_file.pk
        else:
            tag['src'] = BROKEN_IMAGE
            tag[ID_FIELD] = 'broken'

    return str(soup)

