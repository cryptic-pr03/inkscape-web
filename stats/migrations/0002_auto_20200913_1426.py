# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2020-09-13 14:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cachingaccess',
            name='size',
            field=models.BigIntegerField(default=0),
        ),
    ]
